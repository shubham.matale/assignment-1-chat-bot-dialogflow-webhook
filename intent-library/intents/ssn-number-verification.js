/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */



const firestoreService = require('../../helper/firestore');

const getAccountNumberIntent = async (df,req = {}) =>{
    firestoreService.initialize()
    let ssnNumber = (+req.body.queryResult.parameters['ssn-number']);
    let accountNumber = (+req.body.queryResult.outputContexts[0]['parameters']['account-number']);
    console.log(ssnNumber)
    if(isNaN(ssnNumber)){
        df.setResponseText("Please enter a valid SSN Number.");
    }

    let data;
    data = await firestoreService.getAccountDataWithSSN('usersAccount',accountNumber,ssnNumber);
    console.log(data)
    if(data.ok){
        df.setResponseText("Thank you for confirming! You currently have $"+data['data'][0]['accountBalance']+"  in your account. ");
    }else{
        df.setResponseText("Sorry. Wrong SSN number for account "+accountNumber);
    }



};

module.exports = getAccountNumberIntent;
