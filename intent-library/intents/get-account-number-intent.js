/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */



const firestoreService = require('../../helper/firestore');

const getAccountNumberIntent = async (df,req = {}) =>{
    firestoreService.initialize()
    console.log(df._request.queryResult.parameters)
    let accountNumber = (+req.body.queryResult.parameters['account-number'])
    console.log(accountNumber)
    if(isNaN(accountNumber)){
        df.setResponseText("Please enter a valid account number.");
        df.setEvent('wrongAccountNumber');
        return;
    }
    console.log(accountNumber)
    let data;
    data = await firestoreService.getAccountData('usersAccount',accountNumber);
    console.log(data)
    if(data.ok){
        console.log("sending response")
        df.setResponseText("Can you tell me the last 4 digits of your SSN number?");
    }else{
        df.setResponseText("Sorry. I am not able to find account with number as "+accountNumber);
        df.setEvent('wrongAccountNumber');
    }



};

module.exports = getAccountNumberIntent;
