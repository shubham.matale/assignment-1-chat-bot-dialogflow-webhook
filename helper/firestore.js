const fs = require('firebase-admin');
const serviceAccount = require('../config/firebase-key.json');
const url = require('url');
let firestoreDb = null;

const generateResponse = (ok, data, status, statusText, request) => ({
    ok,
    data,
    status,
    statusText,
    request
});

const STATUS = {
    OK: 'OK',
    FAILURE: 'Failure'
};

const SUCCESS_CODES = {
    OK: 200,
    CREATED: 201,
    NO_CONTENT: 204
};

const CLIENT_ERROR_CODES = {
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    CONFLICT: 409
};

const REQUEST = {
    INITIALIZE: 'INITIALIZE',
    CREATE: 'CREATE',
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE',
    PUT: 'PUT',
    LOGIN: 'LOGIN',
    SIGN_UP: 'SIGN_UP',
    UPDATE_PROFILE: 'UPDATE_PROFILE'
};

function initializeFirestore() {
    try {
        fs.initializeApp({
            credential: fs.credential.cert(serviceAccount)
        });
        firestoreDb = fs.firestore();
        return generateResponse(true, firestore, SUCCESS_CODES.OK, STATUS.OK, REQUEST.INITIALIZE);
    } catch (error) {
        return generateResponse(false, error, CLIENT_ERROR_CODES.BAD_REQUEST, STATUS.FAILURE, REQUEST.INITIALIZE);
    }
}

async function getAccountData( collectionName , accountNo) {
    try {

        let data = await firestoreDb.collection(collectionName).where('accountNumber','==',accountNo).get().then(snapshot =>
            snapshot.docs.map(doc => ({ ...doc.data(), id: doc.id })));
        console.log(data)
        if(data.length>0){
            return generateResponse(true, data, SUCCESS_CODES.OK, STATUS.OK, REQUEST.GET);
        }else{
            return generateResponse(false, {error:"No record Found"}, SUCCESS_CODES.NO_CONTENT, STATUS.FAILURE, REQUEST.GET);
        }

    } catch (error) {
        return generateResponse(false, error, CLIENT_ERROR_CODES.BAD_REQUEST, STATUS.FAILURE, REQUEST.GET);
    }
}

async function getAccountDataWithSSN( collectionName , accountNo, ssn) {
    try {

        let data = await firestoreDb.collection(collectionName).where('accountNumber','==',accountNo).where('ssn','==',ssn).limit(1).get().then(snapshot =>
            snapshot.docs.map(doc => ({ ...doc.data(), id: doc.id })));
        if(data.length>0){
            return generateResponse(true, data, SUCCESS_CODES.OK, STATUS.OK, REQUEST.GET);
        }else{
            return generateResponse(false, {error:"No record Found"}, SUCCESS_CODES.NO_CONTENT, STATUS.FAILURE, REQUEST.GET);
        }

    } catch (error) {
        return generateResponse(false, error, CLIENT_ERROR_CODES.BAD_REQUEST, STATUS.FAILURE, REQUEST.GET);
    }
}



const firestoreService = {
    initialize: () => initializeFirestore(),
    getAccountData: (collectionName, accountNo) => getAccountData(collectionName, accountNo),
    getAccountDataWithSSN:(collectionName, accountNo,ssn)=>getAccountDataWithSSN(collectionName, accountNo,ssn)
};

module.exports =  firestoreService;